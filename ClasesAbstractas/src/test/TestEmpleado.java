/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import clasesabstractas.EmpleadoSueldo;
import clasesabstractas.EmpleadoPorComision;


/**
 *
 * @author Lia
 */
public class TestEmpleado {

  
    public static void main(String[] args) {
      
      EmpleadoSueldo emple = new EmpleadoSueldo(13456433,"Juan", "Perez",400,2650);
      EmpleadoPorComision emplecomi = new EmpleadoPorComision(19448448,"Jose", "Marcano",350,48000,10);
      emple.imprimeAsalariado();
      emplecomi.imprimePorComision();

    }
}


