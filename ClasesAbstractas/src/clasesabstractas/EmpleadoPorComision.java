/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lia
 */
package clasesabstractas;

public class EmpleadoPorComision extends Empleado{

    private float ventas;
    private float comision;

    public EmpleadoPorComision( int cedula, String nombre, String apellido, float hcm,float ventas, float comision) {
        super(cedula, nombre, apellido, hcm);
        this.ventas = ventas;
        this.comision = comision;
    }

    public float ingresos()
    {
        return (ventas * comision)/100;
    }
    
    public float netoACobrar(){
        return  ingresos() - super.getHcm();
     } 
    
   public void setVentas(float ventas)
    {
     ventas= ventas < 0 ? 0 : ventas;
    }

    public float getVentas()
     {
       return ventas;
     }

    public void setComision(float comision)
    {
     comision = ( comision > 0 && comision <= 10 ) ? comision : 0;
    }

    public float getComision()
     {
       return comision;
     }

   

     public void imprimePorComision()
      { System.out.println("Empleado por Comision");
        System.out.println("---------------------");
        super.imprimeEmpleado();
        System.out.println( "Ingresos: " + ingresos() + ","+ "  Neto a Cobrar:  " + netoACobrar() );
       }
}
