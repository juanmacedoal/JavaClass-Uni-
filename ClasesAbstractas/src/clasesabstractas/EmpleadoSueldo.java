/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lia
 */
package clasesabstractas;

public class EmpleadoSueldo extends Empleado  {

 private float sueldo;

 public EmpleadoSueldo( int cedula, String nombre, String apellido, float hcm, float sueldo)
 {
     super(cedula,nombre,apellido,hcm);
     this.sueldo=sueldo;
 }

 public float netoACobrar(){
    return sueldo - super.getHcm();
  }

 public void setSueldo(float sueldo)
 {
     sueldo= sueldo < 0 ? 0 : sueldo;
 }

 public float getSueldo()
 {
     return sueldo;
 }


 public void imprimeAsalariado()
   { System.out.println("Empleado Asalariado");
     System.out.println("-------------------");
     super.imprimeEmpleado();
     System.out.println( "Sueldo: " + sueldo + ","+ "  Neto a Cobrar:  " + netoACobrar() );
   }
}
