/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lia
 */

package clasesabstractas;

public abstract class Empleado
{
   protected int cedula;
   protected String nombre;
   protected String apellido;
   protected float hcm;

  public Empleado(int cedula, String nombre, String apellido, float hcm) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.hcm = hcm;    }

  public void imprimeEmpleado()
   {  System.out.println( "Cedula: " + cedula + "," + "  Nombre: " + nombre + "," + "  Apellido: " + apellido);
      System.out.println( "Descuentos:  Seguro HCM -> " + hcm);   }

  public abstract float netoACobrar();
// este es el método abstracto que debe ser implementado por la subclase

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public float getHcm() {
        return hcm;
    }

    public void setHcm(float hcm) {
        this.hcm = hcm;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
  
   
  
}

