/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lia
 */
package archivossecuenciales;
import java.io.FileNotFoundException;
import java.util.*;

public class CrearArchivoTexto {
  private Formatter salida; 

   public void abrirArchivo()
   {
      try
      {
         salida = new Formatter( "src/persistencia/ArchivoEstudiantes.txt" );
      }
     
      catch ( FileNotFoundException filesNotFoundException )
      {
         System.err.println( "Error al crear el archivo "+filesNotFoundException );
         System.exit( 1 );
      } 
   } 

   public void agregarRegistros(){
    Estudiante registro = new Estudiante();
    Scanner entrada = new Scanner( System.in );
    Scanner datos = new Scanner( System.in );
    System.out.println("Indique numero de elementos a colocar en el archivo: ");
    int n = datos.nextInt();
    System.out.println( "Escriba el numero de Cedula (> 0), Nombre, Apellido y Promedio de Notas (00 a 20): \n" );

    boolean continuarCiclo = true; int i=0;
    do {
     try 
      {             
       registro.setCedula( entrada.nextInt() );
       registro.setNombre( entrada.next() );
       registro.setApellido( entrada.next() );
       registro.setPromedio( entrada.nextDouble() );
       if (registro.getCedula()> 0 && registro.getPromedio()>= 0 && registro.getPromedio()<= 20)
        { // escribe el nuevo registro
             salida.format( "%d %s %s %.2f\n", registro.getCedula(),
             registro.getNombre(), registro.getApellido(),
             registro.getPromedio() );
             i++;
             if (i==n)
                   continuarCiclo=false;
         }
        else
         {
           if  (registro.getCedula() <= 0)
                 System.out.println("El numero de cedula debe ser mayor que 0." );
          if (registro.getPromedio()< 0 || registro.getPromedio() > 20)
                 System.out.println("El promedio debe estar entre 0 y 20 puntos" );
         }
      }
      catch ( InputMismatchException elementException )
      {
       System.err.println( "Entrada invalida. Intente de nuevo, Tipo de Error ="+elementException );
       entrada.nextLine();
       System.out.println( "Escriba el numero de Cedula (> 0), Nombre, Apellido y Promedio de Notas (00 a 20): \n" );
      }

    }  while (continuarCiclo);
        
   } 
    

   public void cerrarArchivo()
   {
      if ( salida != null )
         salida.close();
   }


}