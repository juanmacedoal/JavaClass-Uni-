/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package archivossecuenciales;

/**
 *
 * @author Lia
 */
public class Estudiante {
   private int cedula;
   private String nombre;
   private String apellido;
   private double promedio;

   public Estudiante() {
        this.cedula = 0;
        this.nombre = "";
        this.apellido = "";
        this.promedio = 0.0;
    }
  
  public Estudiante(int cedula, String nombre, String apellido) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
    }
  public void setCedula( int cedula )
   {
      this.cedula = cedula;
   }

   public int getCedula()
   {
      return cedula;
   }

   public void setNombre( String nombre )
   {
      this.nombre = nombre;
   }

   public String getNombre()
   {
      return nombre;
   }

   public void setApellido( String apellido )
   {
      this.apellido = apellido;
   }

   public String getApellido()
   {
      return apellido;
   }

   public void setPromedio( double promedio )
   {
      this.promedio = promedio;
   }

   public double getPromedio()
   {
      return promedio;
   }

    
}
