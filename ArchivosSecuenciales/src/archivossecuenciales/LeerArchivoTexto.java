/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package archivossecuenciales;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Lia
 */
public class LeerArchivoTexto {
   private Scanner entrada;

   // permite al usuario abrir el archivo
   public void abrirArchivo()
   {
    try
     {
       entrada = new Scanner( new File( "ArchivoEstudiantes.txt" ) );
     }
    catch ( FileNotFoundException fileNotFoundException )
     {
      System.err.println( "Error archivo no existe, debe crearlo primero, tipo de error = "+fileNotFoundException );
      System.exit(1);
     }
   }

  
   public void leerRegistros()
   {  Estudiante registro = new Estudiante();
      System.out.printf( "%-9s%-15s%-18s%10s\n", "Cedula",
         "Nombre", "Apellido", "Promedio de Notas" );
      try
      { while ( entrada.hasNext() )
         {  registro.setCedula( entrada.nextInt() );
            registro.setNombre( entrada.next() );
            registro.setApellido( entrada.next() );
            registro.setPromedio( entrada.nextDouble() );
    
            System.out.printf( "%-9d%-15s%-18s%10.2f\n",
               registro.getCedula(), registro.getNombre(),
               registro.getApellido(), registro.getPromedio() );
         }
      }
      catch ( NoSuchElementException elementException )
      { System.err.println( "El archivo no esta bien formateado "+elementException );
         entrada.close();
         System.exit( 1 );
      }
      catch ( IllegalStateException stateException )
      {  System.err.println( "Error al abrir el archivo "+stateException );
         System.exit( 1 );
      }
   }

  public void cerrarArchivo()
   {
      if ( entrada != null )
         entrada.close();
   }
}
