/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lia
 */
package archivossecuenciales;
import java.util.Scanner;

public class TestArchivodeTexto {
 
    public static void main(String[] args) {
      Scanner dato = new Scanner( System.in );
      System.out.println("Indique la opcion requerida 1=Crear Archivo, 2=Leer Archivo: \n");
      int opcion = dato.nextInt();
      switch(opcion){
          case 1:  CrearArchivoTexto crear = new CrearArchivoTexto();
                   crear.abrirArchivo();
                   crear.agregarRegistros();
                   crear.cerrarArchivo();
                   break;
          case 2:  LeerArchivoTexto leer = new LeerArchivoTexto();
                   leer.abrirArchivo();
                   leer.leerRegistros();
                   leer.cerrarArchivo();
                   break;
      }
    }
    
    
    

}
