/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lia
 */

package interfaz;
import controller.Controladora;
import dominio.Persona;
import dominio.Telefono;
import javax.swing.ButtonGroup;

public class VentanaAgregarTelefono extends javax.swing.JFrame {
     
   Persona personaActual;
   Telefono telefonoActual;
   ButtonGroup grupo = new ButtonGroup(); // crea ButtonGroup 
                                     // para seleccionar una única opción
                                     //entre los RadioButton creados
   Controladora control;
       
    /** Creates new form VentanaAgregarTelefono */
    public VentanaAgregarTelefono() {
         initComponents();
         Controladora controlInicio = new Controladora(this);
         controlInicio.iniciaVentana(this, "src/imagenes/nokia.png");
         control = new Controladora(comboCodigosArea);
         control.llenarCombo();
         crearGrupoOpciones();
    }

    public VentanaAgregarTelefono(Persona perAct) {
        initComponents();
         Controladora controlInicio = new Controladora(this);
         controlInicio.iniciaVentana(this, "src/imagenes/nokia.png");
         nomApe.setText(perAct.getNombres());//El Label se inicializa con la
         personaActual = perAct;             //persona actual
         control = new Controladora(comboCodigosArea);
         control.llenarCombo();
         crearGrupoOpciones();
    }

    private void crearGrupoOpciones()//método privado únicamente definido para ser utilizado dentro de la clase. 
      { grupo.add(movil); 
        grupo.add(casa); 
        grupo.add(oficina); 
      }   
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        datosLabel = new javax.swing.JLabel();
        icono = new javax.swing.JLabel();
        volver = new javax.swing.JButton();
        registrar = new javax.swing.JButton();
        codigoArea = new javax.swing.JLabel();
        numero = new javax.swing.JLabel();
        TipoTelefono = new javax.swing.JLabel();
        numeroTxt = new javax.swing.JTextField();
        nomApe = new javax.swing.JLabel();
        comboCodigosArea = new javax.swing.JComboBox();
        casa = new javax.swing.JRadioButton();
        movil = new javax.swing.JRadioButton();
        oficina = new javax.swing.JRadioButton();
        fondo = new javax.swing.JLabel();
        fondo1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Información de Teléfonos");
        setAlwaysOnTop(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        datosLabel.setText("Teléfono de la Persona: ");
        jPanel1.add(datosLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 140, -1));

        icono.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        icono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tele2.png"))); // NOI18N
        jPanel1.add(icono, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 80, 60));

        volver.setText("Volver");
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        jPanel1.add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 150, 140, 36));

        registrar.setText("Registrar Teléfono");
        registrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarActionPerformed(evt);
            }
        });
        jPanel1.add(registrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 70, 140, 36));

        codigoArea.setText("Código de Área");
        jPanel1.add(codigoArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, -1, -1));

        numero.setText("Número ");
        jPanel1.add(numero, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, -1, -1));

        TipoTelefono.setText("Tipo de Teléfono");
        jPanel1.add(TipoTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, 100, 20));

        numeroTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numeroTxtActionPerformed(evt);
            }
        });
        jPanel1.add(numeroTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 110, 130, -1));

        nomApe.setText("Nombre y Apellido");
        jPanel1.add(nomApe, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 20, 130, -1));

        comboCodigosArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCodigosAreaActionPerformed(evt);
            }
        });
        jPanel1.add(comboCodigosArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 130, -1));

        casa.setText("Casa");
        jPanel1.add(casa, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 150, -1, -1));

        movil.setText("Celular");
        jPanel1.add(movil, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 170, -1, -1));

        oficina.setText("Oficina");
        jPanel1.add(oficina, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 190, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 500, 250));

        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/FONDO INICIO.png"))); // NOI18N
        getContentPane().add(fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 560, 390));

        fondo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/FONDO INICIO.png"))); // NOI18N
        getContentPane().add(fondo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 560, 380));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        // TODO add your handling code here:
        VentanaAgregarPersona ventana = new VentanaAgregarPersona();
        ventana.setLocationRelativeTo(null);
        ventana.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_volverActionPerformed

    private void registrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarActionPerformed
         
          telefonoActual = new Telefono();
          control = new Controladora(comboCodigosArea,numeroTxt);
          control.datosTelefono(telefonoActual,comboCodigosArea,numeroTxt,movil,casa,oficina);
          personaActual.setTelefono(telefonoActual);
           
    }//GEN-LAST:event_registrarActionPerformed

    private void numeroTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numeroTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numeroTxtActionPerformed

private void comboCodigosAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCodigosAreaActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_comboCodigosAreaActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaAgregarTelefono().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel TipoTelefono;
    private javax.swing.JRadioButton casa;
    private javax.swing.JLabel codigoArea;
    private javax.swing.JComboBox comboCodigosArea;
    private javax.swing.JLabel datosLabel;
    private javax.swing.JLabel fondo;
    private javax.swing.JLabel fondo1;
    private javax.swing.JLabel icono;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton movil;
    private javax.swing.JLabel nomApe;
    private javax.swing.JLabel numero;
    private javax.swing.JTextField numeroTxt;
    private javax.swing.JRadioButton oficina;
    private javax.swing.JButton registrar;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables

}
