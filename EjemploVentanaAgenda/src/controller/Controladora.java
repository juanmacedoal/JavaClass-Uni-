/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dominio.*;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

/**
 *
 * @author Lia
 */
public class Controladora {
    
    JButton listaPersonas,agregarNumero;
    JTextField cedulaTxt,nombresTxt,emailTxt,numeroTxt;
    JComboBox comboCodigosArea;
    JFrame ventana;
    
    public Controladora(JFrame ventana){
       this.ventana = ventana;
    }
    public Controladora(JButton listaPersonas,JButton agregarNumero){
        this.listaPersonas = listaPersonas;
        this.agregarNumero = agregarNumero;
    }
    
    public Controladora(JTextField cedulaTxt,JTextField nombresTxt, JTextField emailTxt,JButton listaPersonas,JButton agregarNumero){
        this.cedulaTxt = cedulaTxt;
        this.nombresTxt = nombresTxt;
        this.emailTxt = emailTxt;
        this.listaPersonas = listaPersonas;
        this.agregarNumero = agregarNumero;
     }
    
    public Controladora(JComboBox comboCodigosArea){
        this.comboCodigosArea = comboCodigosArea;
    }
    
    public Controladora(JComboBox comboCodigosArea, JTextField numeroTxt){
        this.comboCodigosArea = comboCodigosArea;
        this.numeroTxt = numeroTxt;
    }
    
    public void iniciaVentana(JFrame ventana, String ruta){
       ventana.setLocationRelativeTo(null); //permite centrar la ventana
       ventana.setIconImage(new ImageIcon(ruta).getImage()); 
      //ImageIcon permite agregar un icono en el frame superior (hacia la
      //izquierda) de la ventana para evitar que salga la taza de java por 
      //defecto, debe importarse la librería javax.swing.ImageIcon
       ventana.setResizable(false); // no permite que el usuario cambie el tamaño 
      // de la ventana, de lo contrario los componentes pueden verse afectados
       ventana.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
         //para evitar cerrar con la "X", solo se sale de la ventana a través
        // del botón Salir del Sistema.
    }
    public void activa_Desactiva(boolean verdadOFalso){
      listaPersonas.setEnabled(verdadOFalso);
      agregarNumero.setEnabled(verdadOFalso);
    }
    
    public void inicializarTxt(JTextField cedulaTxt,JTextField nombresTxt, JTextField emailTxt){
      cedulaTxt.setText(null);
      nombresTxt.setText(null);
      emailTxt.setText(null);
    
     }
    
    public void datosPersona(Agenda regPer,Persona personaActual){
        personaActual.setNombres(nombresTxt.getText());
        //getText permite tomar el contenido de un componente TextField
        personaActual.setCedula(Long.parseLong(cedulaTxt.getText()));
        //Long.parseLong sirve para convertir un string en entero largo
        personaActual.setEmail(emailTxt.getText());
        regPer.setPersona(personaActual); 
    }
    
    public void datosTelefono(Telefono telefonoActual, JComboBox comboCodigosArea, JTextField numeroTxt,JRadioButton movil,JRadioButton casa,JRadioButton oficina ){
        telefonoActual.setNumero(Long.parseLong(numeroTxt.getText()));
        telefonoActual.setCodigo(comboCodigosArea.getSelectedItem().toString()); //getSelectedItem toma el 
                                               //contenido del valor seleccionado en el combo por parte del usuario 
        if (movil.isSelected()==true)  //isSelected identifica cuál de los radioButton fue seleccionado por el usuario
              telefonoActual.setTipoTel("Celular");
        if (casa.isSelected()==true)
              telefonoActual.setTipoTel("Casa");
        if (oficina.isSelected()==true)
              telefonoActual.setTipoTel("Oficina");
    }
    
    public void llenarCombo()
      { 
        comboCodigosArea.addItem(212);
        comboCodigosArea.addItem(414);
        comboCodigosArea.addItem(424);
        comboCodigosArea.addItem(416);
        comboCodigosArea.addItem(426);
        comboCodigosArea.addItem(412);
        comboCodigosArea.addItem(243); 
      }
}
