/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lia
 */
package dominio;

public class Agenda {
//se trabajara con un vector por no haber visto aún el tema de Colecciones
    private Persona personas[];
    private int numeroDePersonas;

    public Agenda(Persona[] personas) {
        this.personas = personas;
    }

    public Agenda()
     { personas = new Persona[20];
       numeroDePersonas = 0;
     } 
    
    public Persona getPersona(int i) {
        return personas[i];
    }

    public void setPersona(Persona persona) {
        personas[numeroDePersonas] = persona;
        numeroDePersonas++; 
    }

    public Persona[] getPersonas() {
        return personas;
    }

    public int getNumeroDePersonas() {
        return numeroDePersonas;
    }
   
   


}
