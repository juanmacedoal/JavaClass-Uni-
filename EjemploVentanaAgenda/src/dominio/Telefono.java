/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lia
 */
package dominio;

public class Telefono {

   private String codigo;
   private Long numero;
   private String tipoTel;

    public Telefono(String codigo, Long numero, String tipoTel) {
        this.codigo = codigo;
        this.numero = numero;
        this.tipoTel = tipoTel;
    }

    public Telefono() {
        codigo = "";
        numero = 0L;
        tipoTel = "";
    }

    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getTipoTel() {
        return tipoTel;
    }

    public void setTipoTel(String tipoTel) {
        this.tipoTel = tipoTel;
    }
   


   
   

  

}
