/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lia
 */
package dominio;

public class Persona {
     private long cedula;
     private String nombres;
     private String email;
     private Telefono telefonos[];
     private int numeroDeTelefonos; 
     
    public Persona(long cedula, String nombres, String email, Telefono[] telefonos) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.email = email;
        this.telefonos = telefonos;
    }
   
    public Persona()
    {
        cedula = 0;
        nombres="";
        email="";
        telefonos=null;
        telefonos = new Telefono[10];
        numeroDeTelefonos = 0;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Telefono[] getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(Telefono[] telefonos) {
        this.telefonos = telefonos;
    }

    public void setTelefono(Telefono telefono) {
        telefonos[numeroDeTelefonos] = telefono;
        numeroDeTelefonos++; 
     }

    public int getNroDeTelefonos() {
        return numeroDeTelefonos;
    }

    public void setNroDeTelefonos(int NroDeTelefonos) {
        this.numeroDeTelefonos = NroDeTelefonos;
    }

       
}