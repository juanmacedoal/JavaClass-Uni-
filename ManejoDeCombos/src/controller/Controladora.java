/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author Lia
 */
public class Controladora {
    
     JComboBox cboMun,cboPar;

     public Controladora(JComboBox cboMun, JComboBox cboPar) { 
            this.cboMun = cboMun;
            this.cboPar = cboPar;
            
     }         
     
   public void iniciaVentana(JFrame ventana, String ruta){
       ventana.setResizable(false);
       ventana.setLocationRelativeTo(null);
       ventana.setIconImage(new ImageIcon(ruta).getImage());
   }  
   public void llenar_combo()
    {int opcion = cboMun.getSelectedIndex();
        switch (opcion)
        {case 0: cboPar.removeAllItems();
                 cboPar.addItem("San Juan");
                 cboPar.addItem("El Paraiso");
                 cboPar.addItem("La Candelaria");
                 cboPar.addItem("El Recreo");
                 break;
         case 1: cboPar.removeAllItems();
                 cboPar.addItem("Petare");
                 cboPar.addItem("La Dolorita");
                 cboPar.addItem("Leoncio Martinez");
                 break;   
        
        case 2:  cboPar.removeAllItems();
                 cboPar.addItem("El Cafetal");
                 cboPar.addItem("EL Hatillo");
                 cboPar.addItem("Santa Fe");
                 break;
        case 3:  cboPar.removeAllItems();
                 cboPar.addItem("Chacao");
                 break;   
        
        }
    
    }     
}
