/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

/**
 *
 * @author Lia
 */
public class Persona {

    protected int cedula;
    protected String nomApe;

    public Persona(int cedula, String nomApe) {
        this.cedula = cedula;
        this.nomApe = nomApe;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNomApe() {
        return nomApe;
    }

    public void setNomApe(String nomApe) {
        this.nomApe = nomApe;
    }


}
