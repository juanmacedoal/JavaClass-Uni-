/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

/**
 *
 * @author Lia
 */
public class Estudiante extends Persona{

    private Float eficiencia;

    public Estudiante(int cedula, String nomApe, Float eficiencia) {
        super(cedula,nomApe);
        this.eficiencia = eficiencia;
    }

   

    public Float getEficiencia() {
        return eficiencia;
    }

    public void setEficiencia(Float eficiencia) {
        this.eficiencia = eficiencia;
    }

    

   


}
