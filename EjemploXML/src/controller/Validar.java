
package controller;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Validar {
// Validar es una Clase Utilitaria solo contiene métodos no es empleada para almacenar datos

 public static int validarVacio (JTextField campo, String mensaje){
       if (campo.getText().isEmpty())
          { JOptionPane.showMessageDialog(null, mensaje, "Error falta un dato", JOptionPane.ERROR_MESSAGE); 
            return 0; 
          }
       else
            return 1;
   }  
 
 public static int validarEntero (String numero, String mensaje){
   try{
         int numero1 = Integer.parseInt(numero);
         return 1; 
      }
   catch(NumberFormatException ex){
      JOptionPane.showMessageDialog(null,mensaje,"Tipo De Dato No es un Entero", JOptionPane.ERROR_MESSAGE);
      return 0;
       }
}
 
 public static int validarRango(float numero,String mensaje)
   {
      if (numero < 0 || numero > 1)
        {JOptionPane.showMessageDialog(null,mensaje,"Rango no permitido", JOptionPane.ERROR_MESSAGE);
         return 0;
        }
      else
        return 1;   
   }
 
 public static int validarReal (String numero, String mensaje)
  {
    try{
       float numero1 = Float.parseFloat(numero); 
       return validarRango(numero1,"Eficiencia debe estar entre 0 y 1");
       
      }
   catch(NumberFormatException ex){
    JOptionPane.showMessageDialog(null,mensaje,"Tipo De Dato No es un Real", JOptionPane.ERROR_MESSAGE);
    return 0;
     }
    
 }
 
}
