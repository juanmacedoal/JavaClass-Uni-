package persistencia;

import dominio.Estudiante;
import dominio.Persona;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

/**
 * Clase de persistencia de implementacion DAO con tecnologia
 * XML para el dominio de Estudiante
 */
public class DaoEstudianteXml {
    /** variable que contiene la raiz del documento Xml*/
    private Element root;
    /** variable que contiene la localizacion del archivo xml*/
    private String fileLocation = "src//archivos//estudiantes.xml";
    /**
     * constructor por defecto que inicia los valores para trabajar con el documento
     * xml
     */
    public DaoEstudianteXml() {
        try {
            SAXBuilder builder = new SAXBuilder(false);
            Document doc = null;
            doc = builder.build(fileLocation);
            root = doc.getRootElement();
        } catch (JDOMException ex) {
            System.out.println("No se pudo iniciar la operacion por: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("No se pudo iniciar la operacion por: " + ex.getMessage());
        }
    }
  /*Aqui lleno todo el contexto que le voy a insertar al archivo, es decir
    el nodo proncipal que es Estudiante con los atributos que le corresponden,
    los cuales seria cedula,nombre y apellido,etc*/
    private Element EstudiantetoXmlElement(Estudiante nEstudiante) {
        Element Estudiantetrans = new Element("Estudiante");
        Element cedula = new Element("cedula");
        cedula.setText(Integer.toString(nEstudiante.getCedula()));
        Element nombre = new Element("nombreyapellido");
        nombre.setText(nEstudiante.getNomApe());
        Element eficiencia = new Element("eficiencia");
        eficiencia.setText(Float.toString(nEstudiante.getEficiencia()));
        Estudiantetrans.addContent(cedula);
        Estudiantetrans.addContent(nombre);
        Estudiantetrans.addContent(eficiencia);
        return Estudiantetrans;
    }

   /*Método que retorna un Estudiante. A este metodo se le manda un Element y con
    sus datos se hará los pasos requeridos para crear el Estudiante*/
    private Estudiante EstudianteToObject(Element element) throws ParseException {
        Estudiante nEstudiante = new Estudiante(Integer.parseInt(element.getChildText("cedula")),element.getChildText("nombreyapellido"),
                                        Float.parseFloat(element.getChildText("eficiencia")));
        return nEstudiante;
    }

   /* Operacion para guardar en el documento Xml los cambios efectuados
    * @return true si se cumplio la operacion con exito, false en caso contrario*/
    private boolean updateDocument() {
        try {
            XMLOutputter out = new XMLOutputter(org.jdom.output.Format.getPrettyFormat());
            FileOutputStream file = new FileOutputStream(fileLocation);
            out.output(root, file);
            file.flush();
            file.close();
            return true;
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return false;
        }
    }

   /* Operacion que busca un elemento que cumpla con una condicion en el id del xml
     * @param raiz = raiz del documento xml
     * @param dato = elemento a buscar.
     * @return retorna el elemento si existe con la condicion, en caso contrario retorna null */
    public static Element buscar(List raiz, String dato) {
        Iterator i = raiz.iterator();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            if (dato.equals(e.getChild("cedula").getText())) {
                return e;
            }
        }
        return null;
    }

   // @return valor boleano con la condicion de exito
    public boolean agregarPersona(Persona nEstudiante) {
        boolean resultado = false;
        root.addContent(EstudiantetoXmlElement((Estudiante) nEstudiante));
        resultado = updateDocument();
        return resultado;
    }

    /* @param cedula numero de cedula del Estudiante a buscar
    * @return objeto Estudiante con sus datos segun busqueda*/
    public Estudiante buscarPersona(Integer cedula) {
        Element aux = new Element("Estudiante");
        List Estudiantes = this.root.getChildren("Estudiante");
        while (aux != null) {
            aux = DaoEstudianteXml.buscar(Estudiantes, Integer.toString(cedula));
            if (aux != null) {
                try {
                    return EstudianteToObject(aux);
                } catch (ParseException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
        return null;
    }

    /* @param Estudiante objeto Estudiante a actualizar
     * @return valor booleano con la condicion de exito */
    public boolean actualizarPersona(Persona nEstudiante) {
        boolean resultado = false;
        Element aux = new Element("Estudiante");
        List Estudiantes = this.root.getChildren("Estudiante");
        while (aux != null) {
            aux = DaoEstudianteXml.buscar(Estudiantes, Integer.toString(nEstudiante.getCedula()));
            if (aux != null) {
                Estudiantes.remove(aux);
                resultado = updateDocument();
            }
        }
        agregarPersona(nEstudiante);
        return resultado;
    }

    /* @param cedula cedula del Estudiante a borrar
     * @return valor boleano con la condicion de exito  */
    public boolean borrarPersona(Integer cedula) {
        boolean resultado = false;
        Element aux = new Element("Estudiante");
        List Estudiantes = this.root.getChildren("Estudiante");
        while (aux != null) {
            aux = DaoEstudianteXml.buscar(Estudiantes, Integer.toString(cedula));
            if (aux != null) {
                Estudiantes.remove(aux);
                resultado = updateDocument();
            }
        }
        return resultado;
    }


    /* Para obtener todos las Personas registradas
     * @return ArrayList con todos los objetos Estudiante  */
      
    public ArrayList<Estudiante> todosLosEstudiantes() {
        ArrayList<Estudiante> resultado = new ArrayList<Estudiante>();
        for (Object it : root.getChildren()) {
            Element xmlElem = (Element) it;
            try {
                resultado.add(EstudianteToObject(xmlElem));
            } catch (ParseException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return resultado;
    }


}
